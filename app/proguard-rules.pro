# RETROFIT
-dontnote retrofit2.Platform
-dontwarn retrofit2.Platform$Java8
-keepattributes Signature
-keepattributes Exceptions

# OKIO
-dontwarn okio.**

# OKHTTP
-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** { *;}
-dontwarn okhttp3.**
-keep class okhttp3.** { *;}
-keep class okhttp3.* { *;}