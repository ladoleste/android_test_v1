package br.com.brasilct.githubclient

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import br.com.brasilct.githubclient.Util.withRecyclerView
import br.com.brasilct.githubclient.common.CustomApplication.Companion.apiUrl
import br.com.brasilct.githubclient.features.pullrequests.PullsActivity
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4::class)
class PullTest {

    @Rule
    @JvmField
    val activityRule: ActivityTestRule<PullsActivity> = ActivityTestRule(PullsActivity::class.java, true, false)

    @Test
    fun test_pulls() {

        val server = MockWebServer()
        val java = Util.readFileFromAssets(InstrumentationRegistry.getContext(), "pulls_jdp.json")

        server.enqueue(MockResponse().setBody(java))
        server.start()

        apiUrl = server.url("/test/").toString()

        val intent = Intent()
        intent.putExtra(PullsActivity.OWNER, "owner")
        intent.putExtra(PullsActivity.REPO, "repo")
        activityRule.launchActivity(intent)

        onView(withText("GitHub Client")).check(matches(isDisplayed()))

        onView(withRecyclerView(R.id.rv_listing).atPosition(0))
                .check(matches(hasDescendant(withText("Update SonarCloud badge"))))

        onView(withRecyclerView(R.id.rv_listing).atPosition(1))
                .check(matches(hasDescendant(withText("Dirty Flag pattern #560"))))

        onView(withRecyclerView(R.id.rv_listing).atPosition(2))
                .check(matches(hasDescendant(withText("#473 serveless implementation using aws compute engine and serverless fram…"))))

        onView(withRecyclerView(R.id.rv_listing).atPosition(0))
                .perform(click())
    }
}
