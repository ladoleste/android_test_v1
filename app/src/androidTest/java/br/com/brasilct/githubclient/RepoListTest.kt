package br.com.brasilct.githubclient

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import br.com.brasilct.githubclient.Util.withRecyclerView
import br.com.brasilct.githubclient.common.CustomApplication.Companion.apiUrl
import br.com.brasilct.githubclient.features.repolisting.MainActivity
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4::class)
class RepoListTest {

    @Rule
    @JvmField
    val activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java, true, false)

    @Test
    fun test_repo_list() {

        val server = MockWebServer()
        val java = Util.readFileFromAssets(InstrumentationRegistry.getContext(), "java.json")
        val js = Util.readFileFromAssets(InstrumentationRegistry.getContext(), "javascript.json")
        val node = Util.readFileFromAssets(InstrumentationRegistry.getContext(), "node.js.json")

        server.enqueue(MockResponse().setBody(java))
        server.enqueue(MockResponse().setBody(js))
        server.enqueue(MockResponse().setBody(node))
        server.start()

        apiUrl = server.url("/test/").toString()

        activityRule.launchActivity(Intent())

        onView(withText("GitHub Client")).check(matches(isDisplayed()))

        onView(withText("Java")).check(matches(isDisplayed()))
        onView(withText("JavaScript")).check(matches(isDisplayed()))
        onView(withText("Node.js")).check(matches(isDisplayed()))

        onView(withRecyclerView(R.id.rv_listing).atPosition(0))
                .check(matches(hasDescendant(withText("RxJava"))))

        onView(withRecyclerView(R.id.rv_listing).atPosition(1))
                .check(matches(hasDescendant(withText("java-design-patterns"))))

        onView(withRecyclerView(R.id.rv_listing).atPosition(2))
                .check(matches(hasDescendant(withText("elasticsearch"))))

        onView(withText("JavaScript")).check(matches(isDisplayed())).perform(click())

        onView(withText("Node.js")).check(matches(isDisplayed())).perform(click())

        onView(withText("Java")).check(matches(isDisplayed())).perform(click())

        onView(withRecyclerView(R.id.rv_listing).atPosition(0)).perform(click())
    }
}
