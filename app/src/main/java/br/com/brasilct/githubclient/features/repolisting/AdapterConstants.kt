package br.com.brasilct.githubclient.features.repolisting

/**
 *Created by Anderson on 15/02/2018.
 */
object AdapterConstants {
    const val REPO_ITEM = 0
    const val LOADING_ITEM = 1
}