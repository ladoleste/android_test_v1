package br.com.brasilct.githubclient.features.pullrequests

import android.support.v7.widget.RecyclerView
import br.com.brasilct.githubclient.common.loadImage
import br.com.brasilct.githubclient.common.toString
import br.com.brasilct.githubclient.dto.pulls.PullResponse
import kotlinx.android.synthetic.main.item_pulls.view.*

/**
 *Created by Anderson on 16/02/2018.
 */
class BinderAdapterPulls {

    companion object {
        fun bind(holder: RecyclerView.ViewHolder, item: PullResponse, clickItem: ItemClickPr) {

            holder.itemView.apply {
                tv_author.text = item.user.login
                iv_author.loadImage(item.user.avatarUrl)
                tv_title.text = item.title
                tv_data.text = item.createdAt.toString("dd/MM/yyyy")
                tv_body.text = item.body

                setOnClickListener { clickItem.onItemClick(item.htmlUrl) }
            }
        }
    }
}
