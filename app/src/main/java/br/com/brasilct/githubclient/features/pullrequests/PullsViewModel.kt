package br.com.brasilct.githubclient.features.pullrequests

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import br.com.brasilct.githubclient.api.RetrofitConfig
import br.com.brasilct.githubclient.dto.pulls.PullResponse
import br.com.brasilct.githubclient.features.repolisting.BaseViewModel
import br.com.brasilct.githubclient.model.GitHubService
import timber.log.Timber


/**
 *Created by Anderson on 14/02/2018.
 */
class PullsViewModel(private val gitHubService: GitHubService = RetrofitConfig.gitHubService) : BaseViewModel() {

    private val _data = MutableLiveData<List<PullResponse>>()
    private val _error = MutableLiveData<Throwable>()

    val data: LiveData<List<PullResponse>> = _data
    val error: LiveData<Throwable> = _error

    fun getPulls(owner: String, repo: String) {
        cDispose.add(
                gitHubService.getPullRequests(owner, repo)
                        .subscribe({
                            _data.postValue(it)
                        }, {
                            Timber.e(it)
                            _error.postValue(it)
                        })
        )
    }
}