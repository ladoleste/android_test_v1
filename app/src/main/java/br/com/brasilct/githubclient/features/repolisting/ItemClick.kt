package br.com.brasilct.githubclient.features.repolisting

/**
 *Created by Anderson on 15/02/2018.
 */
interface ItemClick {
    fun onItemClick(owner: String, repo: String)
}