package br.com.brasilct.githubclient.model

import br.com.brasilct.githubclient.dto.pulls.PullResponse
import br.com.brasilct.githubclient.dto.repo.RepoResponse
import br.com.brasilct.githubclient.dto.user.UserResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 *Created by Anderson on 08/12/2017.
 */
interface GitHubService {
    @GET("/search/repositories?sort=stars&per_page=20")
    fun getRepos(@Query("q") lang: String, @Query("page") page: Int): Single<RepoResponse>

    @GET("/users/{user}")
    fun getUserInfo(@Path("user") lang: String): Single<UserResponse>

    @GET("/repos/{owner}/{repo}/pulls")
    fun getPullRequests(@Path("owner") owner: String, @Path("repo") repo: String): Single<List<PullResponse>>
}