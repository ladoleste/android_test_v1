package br.com.brasilct.githubclient.dto.pulls

import com.squareup.moshi.Json

data class Comments(
        @Json(name = "href") val href: String
)