package br.com.brasilct.githubclient.features.repolisting

import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import br.com.brasilct.githubclient.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var sectionsPagerAdapter: SectionsPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        sectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        view_pager.offscreenPageLimit = 2
        view_pager.adapter = sectionsPagerAdapter
        tab_header.setupWithViewPager(view_pager)
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int) = RepoFragment.newInstance(ConfigTabs.items[position].query)

        override fun getPageTitle(position: Int) = ConfigTabs.items[position].name

        override fun getCount(): Int {
            return ConfigTabs.items.size
        }
    }

}
