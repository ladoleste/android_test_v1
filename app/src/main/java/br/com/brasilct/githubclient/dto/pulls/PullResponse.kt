package br.com.brasilct.githubclient.dto.pulls

import com.squareup.moshi.Json
import java.util.*


/**
 * Created by Anderson on 09/03/2018
 */

data class PullResponse(
        @Json(name = "url") val url: String,
        @Json(name = "id") val id: Int,
        @Json(name = "html_url") val htmlUrl: String,
        @Json(name = "diff_url") val diffUrl: String,
        @Json(name = "patch_url") val patchUrl: String,
        @Json(name = "issue_url") val issueUrl: String,
        @Json(name = "number") val number: Int,
        @Json(name = "state") val state: String,
        @Json(name = "locked") val locked: Boolean,
        @Json(name = "title") val title: String,
        @Json(name = "user") val user: User,
        @Json(name = "body") val body: String,
        @Json(name = "created_at") val createdAt: Date,
        @Json(name = "updated_at") val updatedAt: String,
        @Json(name = "closed_at") val closedAt: Any,
        @Json(name = "merged_at") val mergedAt: Any,
        @Json(name = "merge_commit_sha") val mergeCommitSha: String,
        @Json(name = "assignee") val assignee: Any,
        @Json(name = "assignees") val assignees: List<Any>,
        @Json(name = "requested_reviewers") val requestedReviewers: List<Any>,
        @Json(name = "requested_teams") val requestedTeams: List<Any>,
        @Json(name = "labels") val labels: List<Any>,
        @Json(name = "milestone") val milestone: Any,
        @Json(name = "commits_url") val commitsUrl: String,
        @Json(name = "review_comments_url") val reviewCommentsUrl: String,
        @Json(name = "review_comment_url") val reviewCommentUrl: String,
        @Json(name = "comments_url") val commentsUrl: String,
        @Json(name = "statuses_url") val statusesUrl: String,
        @Json(name = "head") val head: Head,
        @Json(name = "base") val base: Base,
        @Json(name = "_links") val links: Links,
        @Json(name = "author_association") val authorAssociation: String
)