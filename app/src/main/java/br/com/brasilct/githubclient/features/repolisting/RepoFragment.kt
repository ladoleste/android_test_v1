package br.com.brasilct.githubclient.features.repolisting

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.brasilct.githubclient.R
import br.com.brasilct.githubclient.features.pullrequests.PullsActivity
import br.com.brasilct.githubclient.features.pullrequests.PullsActivity.Companion.OWNER
import br.com.brasilct.githubclient.features.pullrequests.PullsActivity.Companion.REPO
import kotlinx.android.synthetic.main.fragment_main.*

class RepoFragment : Fragment(), ItemClick {

    private lateinit var model: MainViewModel
    private lateinit var repoAdapter: RepoAdapter
    private lateinit var loadingScrollListener: LoadingScrollListener
    private var page = 0

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        model = ViewModelProviders.of(this).get(MainViewModel::class.java)

        model.data.observe(this, Observer {
            it?.let {
                loading.visibility = View.GONE

                rvListing.clearOnScrollListeners()
                if (it.size >= 20)
                    rv_listing.addOnScrollListener(loadingScrollListener)

                if (rvListing.adapter == null) {
                    repoAdapter = RepoAdapter(it, this)
                    rvListing.adapter = repoAdapter
                    loadingScrollListener.loading = false
                } else {
                    loadingScrollListener.loading = false
                    repoAdapter.updateItems(it)
                }
            }
        })

        model.error.observe(this, Observer {
            it?.let {
                loading.visibility = View.GONE
            }
        })
    }

    override fun onResume() {
        super.onResume()
        load()
    }

    override fun onItemClick(owner: String, repo: String) {
        val intent = Intent(context, PullsActivity::class.java)
        intent.putExtra(OWNER, owner)
        intent.putExtra(REPO, repo)
        startActivity(intent)
    }

    private fun load() {
        val query = arguments?.getString(QUERY) ?: ""
        rvListing.tag = query
        model.getRepos(query, ++page)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_main, container, false)!!

    override fun onDestroyView() {
        super.onDestroyView()
        model.data.removeObservers(this)
    }

    private val rvListing by lazy {
        val linearLayoutManager = LinearLayoutManager(context)
        rv_listing.layoutManager = linearLayoutManager
        loadingScrollListener = LoadingScrollListener({ load() }, linearLayoutManager)
        rv_listing
    }

    companion object {
        private const val QUERY = "query"

        fun newInstance(query: String): RepoFragment {
            val fragment = RepoFragment()
            val args = Bundle()
            args.putString(QUERY, query)
            fragment.arguments = args
            return fragment
        }
    }
}