package br.com.brasilct.githubclient.features.repolisting

/**
 * Created by Anderson on 10/03/2018
 */
object ConfigTabs {
    val items = listOf(
            ConfigItem("Java", "language:Java"),
            ConfigItem("JavaScript", "language:JavaScript"),
            ConfigItem("Node.js", "Node.js")
    )

    data class ConfigItem(val name: String, val query: String)
}