package br.com.brasilct.githubclient.features.pullrequests

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import br.com.brasilct.githubclient.R
import br.com.brasilct.githubclient.common.inflate
import br.com.brasilct.githubclient.dto.pulls.PullResponse

class PullsAdapter(private var items: List<PullResponse>, private val openUrl: ItemClickPr) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.item_pulls))

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = BinderAdapterPulls.bind(holder, items[position], openUrl)

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)
}