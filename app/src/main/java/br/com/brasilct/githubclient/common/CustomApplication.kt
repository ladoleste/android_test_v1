package br.com.brasilct.githubclient.common

import android.app.Application
import br.com.brasilct.githubclient.BuildConfig
import com.facebook.stetho.Stetho
import timber.log.Timber

class CustomApplication : Application() {

    companion object {

        lateinit var instance: CustomApplication
            private set

        var apiUrl: String = BuildConfig.API_URL
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        @Suppress("ConstantConditionIf")
        if (BuildConfig.DEBUG)
            Timber.plant(DebugLog())
        else
            Timber.plant(ReleaseLog())
        instance = this
    }
}