package br.com.brasilct.githubclient.features.pullrequests

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import br.com.brasilct.githubclient.R
import br.com.brasilct.githubclient.common.Util.getBitmapFromVectorDrawable
import br.com.brasilct.githubclient.features.pullrequests.customtabs.CustomTabsHelper
import kotlinx.android.synthetic.main.activity_pulls.*

class PullsActivity : AppCompatActivity(), ItemClickPr {
    private lateinit var model: PullsViewModel
    private lateinit var pullsAdapter: PullsAdapter
    private lateinit var customTabsIntent: CustomTabsIntent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pulls)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        model = ViewModelProviders.of(this).get(PullsViewModel::class.java)

        model.data.observe(this, Observer {
            it?.let {
                loading.visibility = View.GONE
                if (it.isEmpty()) {
                    tv_nothing_found.visibility = VISIBLE
                    rvListing.visibility = GONE
                } else {
                    pullsAdapter = PullsAdapter(it, this)
                    rvListing.adapter = pullsAdapter
                }
            }
        })

        model.error.observe(this, Observer {
            it?.let {
                loading.visibility = View.GONE
            }
        })
    }

    override fun onItemClick(url: String) {
        CustomTabsHelper.openCustomTab(this, customTabsIntent, Uri.parse(url), null)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }

    override fun onResume() {
        super.onResume()
        load()

        val backArrow = getBitmapFromVectorDrawable(R.drawable.ic_arrow_back_white)

        customTabsIntent = CustomTabsIntent.Builder()
                .addDefaultShareMenuItem()
                .setShowTitle(true)
                .setCloseButtonIcon(backArrow)
                .setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setStartAnimations(this, R.anim.slide_in_right, R.anim.slide_out_left)
                .setExitAnimations(this, R.anim.slide_in_left, R.anim.slide_out_right)
                .build()
    }

    private fun load() {
        model.getPulls(intent.getStringExtra(OWNER), intent.getStringExtra(REPO))
    }

    private val rvListing by lazy {
        val linearLayoutManager = LinearLayoutManager(this)
        rv_listing.layoutManager = linearLayoutManager
        rv_listing
    }

    companion object {
        const val OWNER = "owner"
        const val REPO = "repo"
    }
}