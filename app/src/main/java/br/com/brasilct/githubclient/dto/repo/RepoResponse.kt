package br.com.brasilct.githubclient.dto.repo

import com.squareup.moshi.Json

/**
 * Created by Anderson on 09/03/2018
 */

data class RepoResponse(
        @Json(name = "total_count") val totalCount: Int,
        @Json(name = "incomplete_results") val incompleteResults: Boolean,
        @Json(name = "items") val items: List<Item>
)

