package br.com.brasilct.githubclient.features.pullrequests

/**
 *Created by Anderson on 15/02/2018.
 */
interface ItemClickPr {
    fun onItemClick(url: String)
}