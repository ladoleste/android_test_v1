package br.com.brasilct.githubclient.common

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import br.com.brasilct.githubclient.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*

fun ImageView.loadImage(imageUrl: String?) {
    if (imageUrl == null) {
        return
    }
    if (context is Activity) {
        val act = context as Activity
        if (!act.isFinishing && !act.isDestroyed)
            Glide.with(act).load(imageUrl).apply(
                    RequestOptions()
                            .circleCrop()
                            .placeholder(R.drawable.ic_person_black)
                            .error(R.drawable.ic_person_black)
            ).into(this)
    }
}

fun ViewGroup.inflate(layoutId: Int): View = LayoutInflater.from(context).inflate(layoutId, this, false)

fun Throwable?.getErrorMessage() = when (this) {
    is SocketTimeoutException -> R.string.no_connection
    is UnknownHostException -> R.string.no_connection
    else -> R.string.generic_error
}

fun Date.toString(format: String): String {
    val dateFormat = SimpleDateFormat(format, Locale.getDefault())
    return dateFormat.format(this)
}