package br.com.brasilct.githubclient.features.repolisting

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import br.com.brasilct.githubclient.api.RetrofitConfig
import br.com.brasilct.githubclient.model.GitHubService
import timber.log.Timber


/**
 *Created by Anderson on 14/02/2018.
 */
class MainViewModel(private val gitHubService: GitHubService = RetrofitConfig.gitHubService) : BaseViewModel() {

    private val _data = MutableLiveData<List<ItemList>>()
    private val _error = MutableLiveData<Throwable>()

    private val loadingItem = object : ItemList {
        override val getType: Int
            get() = AdapterConstants.LOADING_ITEM
    }

    val data: LiveData<List<ItemList>> = _data
    val error: LiveData<Throwable> = _error

    fun getRepos(query: String, page: Int) {
        cDispose.add(

                gitHubService.getRepos(query, page)
                        .subscribe({

                            val elements = it.items.toMutableList()

                            var list = _data.value?.toMutableList()

                            if (list != null && !list.isEmpty()) {
                                list.remove(list.last())
                                list.addAll(elements)
                            } else {
                                list = elements.toMutableList()
                            }

                            list.add(loadingItem)
                            _data.postValue(list)
                        }, {
                            Timber.e(it)
                            _error.postValue(it)
                        })
        )
    }
}