package br.com.brasilct.githubclient.features.repolisting

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import br.com.brasilct.githubclient.R
import br.com.brasilct.githubclient.common.inflate
import br.com.brasilct.githubclient.dto.repo.Item
import br.com.brasilct.githubclient.features.repolisting.AdapterConstants.LOADING_ITEM
import br.com.brasilct.githubclient.features.repolisting.AdapterConstants.REPO_ITEM

class RepoAdapter(private var items: List<ItemList>, private val itemClick: ItemClick) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int {
        return items[position].getType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            REPO_ITEM -> ViewHolder(parent.inflate(R.layout.item_repos))
            LOADING_ITEM -> ViewHolder(parent.inflate(R.layout.item_loading))
            else -> throw RuntimeException("Unsupported Adapter Type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            REPO_ITEM -> BinderAdapterRepo.bind(holder, items[position] as Item, itemClick)
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)

    fun updateItems(it: List<ItemList>) {
        items = it
        notifyDataSetChanged()
    }
}