package br.com.brasilct.githubclient.api

import br.com.brasilct.githubclient.common.CustomApplication.Companion.apiUrl
import br.com.brasilct.githubclient.model.GitHubService
import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*




/**
 *Created by Anderson on 15/02/2018.
 */
object RetrofitConfig {

    val gitHubService: GitHubService by lazy {

        val moshi = Moshi.Builder()
                .add(customDateAdapter)
                .build()

        val retrofit = Retrofit.Builder()
                .client(OkHttpProvider.okHttpInstance)
                .baseUrl(apiUrl)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()

        retrofit.create(GitHubService::class.java)
    }

    private var customDateAdapter: Any = object : Any() {
        internal val dateFormat: DateFormat

        init {
            dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"))
        }

        @ToJson
        @Synchronized
        internal fun dateToJson(date: Date): String {
            return dateFormat.format(date)
        }

        @FromJson
        @Synchronized
        @Throws(ParseException::class)
        internal fun dateToJson(strDate: String): Date {
            return dateFormat.parse(strDate)
        }
    }
}