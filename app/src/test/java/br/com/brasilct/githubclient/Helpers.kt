package br.com.brasilct.githubclient

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.io.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 *Created by Anderson on 15/02/2018.
 */
object Helpers {

    var customDateAdapter: Any = object : Any() {
        internal val dateFormat: DateFormat

        init {
            dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"))
        }

        @ToJson
        @Synchronized
        internal fun dateToJson(date: Date): String {
            return dateFormat.format(date)
        }

        @FromJson
        @Synchronized
        @Throws(ParseException::class)
        internal fun dateToJson(strDate: String): Date {
            return dateFormat.parse(strDate)
        }
    }

    fun readFile(fileName: String = "response"): String {
        val datax = StringBuffer("")
        try {

            val classLoader = Helpers::class.java.classLoader
            val resource = classLoader.getResource("$fileName.json")
            val file = File(resource!!.path)

            val fIn = FileInputStream(file)
            val isr = InputStreamReader(fIn)
            val buffreader = BufferedReader(isr)

            var readString = buffreader.readLine()
            while (readString != null) {
                datax.append(readString)
                readString = buffreader.readLine()
            }

            isr.close()
        } catch (ioe: IOException) {
            ioe.printStackTrace()
        }

        return datax.toString()
    }
}