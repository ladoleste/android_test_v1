package br.com.brasilct.githubclient

import android.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.brasilct.githubclient.Helpers.readFile
import br.com.brasilct.githubclient.dto.repo.Item
import br.com.brasilct.githubclient.dto.repo.RepoResponse
import br.com.brasilct.githubclient.features.repolisting.MainViewModel
import br.com.brasilct.githubclient.model.GitHubService
import com.squareup.moshi.Moshi
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class RepoTest {

    @Mock
    private lateinit var gitHubService: GitHubService

    @Suppress("unused")
    @get:Rule
    var testRule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainViewModel
    private val url = "http://nu.com/chargeback"

    @Before
    fun setUp() {
        viewModel = MainViewModel(gitHubService)
    }

    @Test
    fun repo_response() {

        val query = "language:Java"

        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<RepoResponse>(RepoResponse::class.java)
        val javaResponse = jsonAdapter.fromJson(readFile("java"))

        `when`(gitHubService.getRepos(query, 1)).thenReturn(Single.just(javaResponse))

        viewModel.getRepos(query, 1)

        assertNull(viewModel.error.value)
        val obj = viewModel.data.value!!

        val item = obj[0] as Item

        assertEquals("RxJava", item.name)
        assertEquals("RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM.", item.description)
        assertEquals(5512, item.forksCount)
        assertEquals(31475, item.stargazersCount)
        assertEquals("ReactiveX", item.owner.login)
    }
}
