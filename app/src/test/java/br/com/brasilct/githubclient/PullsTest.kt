package br.com.brasilct.githubclient

import android.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.brasilct.githubclient.Helpers.customDateAdapter
import br.com.brasilct.githubclient.Helpers.readFile
import br.com.brasilct.githubclient.common.toString
import br.com.brasilct.githubclient.dto.pulls.PullResponse
import br.com.brasilct.githubclient.features.pullrequests.PullsViewModel
import br.com.brasilct.githubclient.model.GitHubService
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class PullsTest {

    @Mock
    private lateinit var gitHubService: GitHubService

    @Suppress("unused")
    @get:Rule
    var testRule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: PullsViewModel

    @Before
    fun setUp() {
        viewModel = PullsViewModel(gitHubService)
    }

    @Test
    fun pull_response() {

        val moshi = Moshi.Builder().add(customDateAdapter).build()
        val type = Types.newParameterizedType(List::class.java, PullResponse::class.java)
        val jsonAdapter = moshi.adapter<List<PullResponse>>(type)
        val pullresponse = jsonAdapter.fromJson(readFile("pulls_jdp"))

        `when`(gitHubService.getPullRequests("owner", "repo")).thenReturn(Single.just(pullresponse))

        viewModel.getPulls("owner", "repo")

        assertNull(viewModel.error.value)
        val obj = viewModel.data.value!!

        val item = obj[0]

        assertEquals("valery1707", item.user.login)
        assertEquals("Update SonarCloud badge", item.title)
        assertEquals("14/03/2018", item.createdAt.toString("dd/MM/yyyy"))
        assertTrue(item.body.startsWith("Update link into badge URL as described in"))
    }
}
